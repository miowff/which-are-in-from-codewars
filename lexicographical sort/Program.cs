﻿using System.Collections.Generic;
class WhichAreIn
{
    public static string[] inArray(string[] array1, string[] array2)
    {
        void AddToResult(string word, string[] substringArray, List<string> result)
        {
            for (int i = 0; i < substringArray.Length; i++)
            {
                if (word.Contains(substringArray[i]))
                {
                    if (result.Contains(substringArray[i]))
                    {
                        continue;
                    }
                    else
                    {
                        result.Add(substringArray[i]);
                    }
                }
            }
        }
        List<string> result = new List<string>();
        foreach (string arrayString in array2)
        {
            AddToResult(arrayString, array1, result);
        }
        result.Sort();
        return result.ToArray();
    }
}